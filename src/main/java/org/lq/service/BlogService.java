package org.lq.service;

import org.lq.entity.Blog;
import org.lq.entity.Type;
import org.lq.vo.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface BlogService {
    Blog getBlog(Long id);

    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    Blog saveBlog(Blog blog);

    Blog updateBlog(Blog blog, Long id);

    void deleteBlog(Long id);

    Page<Blog> listBlog(Pageable pageable);

    Page<Blog> listBlog(Long tagId,Pageable pageable);
    List<Blog> listBlogRecommendTop(Integer size);

    Page<Blog> listBlog(String query, Pageable pageable);

    Blog getAndConvert(Long id);

    //归档map集合
    Map<String,List<Blog>> archiveBlog();

    Long countBlog();

}
