package org.lq.service;

import org.lq.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface TagService {
    Tag saveTag(Tag Tag);

    Tag getTagById(Long id);

    Page<Tag> listTag(Pageable pageable);

    Tag updateTag(Long id, Tag Tag);

    void deleteTag(Long id);

    Tag getTagByName(String name);

    List<Tag> findAll();

    List<Tag> listTag(String ids);

    List<Tag> listTagTop(Integer size);

}
