package org.lq.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.Md5Crypt;
import org.lq.dao.UserRepository;
import org.lq.entity.User;
import org.lq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public User checkUser(String username, String password) {
        String md5Hex = DigestUtils.md5Hex(password + '.' + username);
        User user = userRepository.findUserByUsernameAndPassword(username, md5Hex);
        return user;
    }

}
