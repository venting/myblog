package org.lq.service.impl;

import org.lq.dao.BlogRepository;
import org.lq.entity.Blog;
import org.lq.entity.Type;
import org.lq.handler.MyFileNotFound;
import org.lq.service.BlogService;
import org.lq.util.MarkdownUtils;
import org.lq.vo.BlogQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.*;

@Service
public class BlogServiceImpl implements BlogService {
    @Autowired
    private BlogRepository blogRepository;
    @Override
    public Blog getBlog(Long id) {
        return blogRepository.findOne(id);
    }

    @Override
    //动态条件组合查询
    public Page<Blog> listBlog(Pageable pageable, BlogQuery blog) {
        return blogRepository.findAll(new Specification<Blog>() {
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();
                //根据标题查询
                if((!"".equals(blog.getTitle()))&&(blog.getTitle()!=null)){
                    predicates.add(cb.like(root.<String>get("title"),"%"+blog.getTitle()+"%"));
                }
                //根据类型查询
                if(blog.getTypeId()!=null){
                    predicates.add(cb.equal(root.<Type>get("type").get("id"), blog.getTypeId()));
                }
                if(blog.isRecommend()){
                    predicates.add(cb.equal(root.<Boolean>get("recommend"), blog.isRecommend()));
                }
                cq.where(predicates.toArray(new Predicate[predicates.size()]));
                return null;
            }
        },pageable);
    }

    @Override
    @Transactional
    public Blog saveBlog(Blog blog) {
        if(blog.getId()==null){
            blog.setCreateTime(new Date());
            blog.setViews(0);
            blog.setUpdateTime(new Date());
            return blogRepository.save(blog);
        }else {
            Blog b = blogRepository.getOne(blog.getId());
            Blog b2 = new Blog();
            BeanUtils.copyProperties(b,b2);
            b2.setUpdateTime(new Date());
            return blogRepository.save(b2);
        }


    }

    @Override
    @Transactional
    public Blog updateBlog(Blog blog, Long id) {
        Blog b = blogRepository.findOne(id);
        if(b==null){
             throw new MyFileNotFound("该博客信息不存在");
        }
        BeanUtils.copyProperties(b,blog);
        return blogRepository.save(b);

    }

    @Override
    @Transactional
    public void deleteBlog(Long id) {
       blogRepository.delete(id);
    }

    @Override
    public Page<Blog> listBlog(Pageable pageable) {
            return blogRepository.findAll(pageable);
    }

    @Override
    public Page<Blog> listBlog(Long tagId, Pageable pageable) {
        return blogRepository.findAll(new Specification<Blog>() {
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Join join = root.join("tags");
                return cb.equal(join.get("id"), tagId);

            }
        },pageable);
    }

    @Override
    public List<Blog> listBlogRecommendTop(Integer size) {
        Sort sort = new Sort(Sort.Direction.DESC, "updateTime");
        Pageable pageable = new PageRequest(0, size, sort);
        return blogRepository.findTop(pageable);
    }

    @Override
    public Page<Blog> listBlog(String query, Pageable pageable) {

        return blogRepository.findByQuery(query,pageable);
    }
    @Transactional
    @Override
    public Blog getAndConvert(Long id) {
        Blog blog = blogRepository.findOne(id);
        if(blog==null){
            throw new MyFileNotFound("博客不存在");
        }
        Blog b = new Blog();
        BeanUtils.copyProperties(blog,b);
        String content = b.getContent();
        b.setContent(MarkdownUtils.markdownToHTMLExtensions(content));
        blogRepository.updateViews(id);
        return b;
    }

    @Override
    public Map<String, List<Blog>> archiveBlog() {
        List<String> years = blogRepository.findGroupYear();
        Map<String, List<Blog>> map = new HashMap<>();
        for(String year : years){
            map.put(year, blogRepository.findByYear(year));
        }
        return map;
    }

    @Override
    public Long countBlog() {
        return blogRepository.count();
    }

}
