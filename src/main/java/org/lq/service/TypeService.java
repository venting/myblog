package org.lq.service;

import org.lq.entity.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface TypeService {
    Type saveType(Type type);

    Type getTypeById(Long id);

    Page<Type> listType(Pageable pageable);

    Type updateType(Long id, Type type);

    void deleteType(Long id);

    Type getTypeByName(String name);

    List<Type> findAllTypes();

    List<Type> listTypeTop(Integer size);

    List<Type> findAllExclusionBlog();
}
