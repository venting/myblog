package org.lq.service;

import org.lq.entity.User;

public interface UserService {
    //检测用户名和密码
    User checkUser(String username, String password);
}
