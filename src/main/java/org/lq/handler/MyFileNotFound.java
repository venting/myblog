package org.lq.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)//加入这个注解springboot会自动跳转到404页面
public class MyFileNotFound extends RuntimeException  {

    public MyFileNotFound() {
        super();
    }


    public MyFileNotFound(String message) {
        super(message);
    }


    public MyFileNotFound(String message, Throwable cause) {
        super(message, cause);
    }
}
