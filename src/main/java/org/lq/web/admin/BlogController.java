package org.lq.web.admin;

import org.lq.entity.Blog;
import org.lq.entity.Type;
import org.lq.entity.User;
import org.lq.service.BlogService;
import org.lq.service.TagService;
import org.lq.service.TypeService;
import org.lq.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class BlogController {
    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;
    private static final String INPUT = "admin/blogs-input";
    private static final String LIST = "admin/blogs";
    private static final String REDIRECT_LIST = "redirect:blogs";
    @GetMapping("/blogs")
    public String getBlogs(@PageableDefault(size = 5,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable , BlogQuery blog, Model model){
        model.addAttribute("page", blogService.listBlog(pageable, blog));
        model.addAttribute("types", typeService.findAllTypes());
        return LIST;
    }
    @PostMapping("/blogs/search")
    public String search(@PageableDefault(size = 3,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable , BlogQuery blog, Model model){
        model.addAttribute("page", blogService.listBlog(pageable, blog));
        return "admin/blogs :: blogList";
    }
    @GetMapping("/blogs/input")
    public String input(Model model){
        //默认使用id为1的类型
        Type type = typeService.getTypeById(1L);
        Blog b = new Blog();
        b.setType(type);
        model.addAttribute("blog", b);
        model.addAttribute("types", typeService.findAllTypes());
        model.addAttribute("tags", tagService.findAll());
        return INPUT;
    }
    //修改博客
    @GetMapping("/blogs/input/{id}")
    public String editInput(@PathVariable Long id, Model model){
        Blog b = blogService.getBlog(id);
        b.init();
        model.addAttribute("blog", b);
        model.addAttribute("types", typeService.findAllTypes());
        model.addAttribute("tags", tagService.findAll());
        return INPUT;
    }
    @PostMapping("/blogs")
    public String post(Blog blog, RedirectAttributes attributes,HttpSession session){
        blog.setUser((User) session.getAttribute("user"));
        blog.setType(typeService.getTypeById(blog.getType().getId()));
        blog.setTags(tagService.listTag(blog.getTagIds()));
        if(blog.getId()==null){
            Blog b = blogService.saveBlog(blog);
            if(b==null){
                attributes.addFlashAttribute("message","操作失败");
            }else{
                attributes.addFlashAttribute("message","操作成功");
            }

        }else{
            blogService.updateBlog(blog, blog.getId());
        }

        return REDIRECT_LIST;
    }
    @GetMapping("/blogs/{id}/delete")
    public String delete(@PathVariable Long id, RedirectAttributes attributes){
        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message", "删除成功");
        return "redirect:/admin/blogs";
    }


}
