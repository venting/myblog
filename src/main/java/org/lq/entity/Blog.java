package org.lq.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="t_blog")
public class Blog {
    @Id//主键
    @GeneratedValue//自动生成
    private Long    id;
    private String  title;
    @Lob
    private String  content;
    private String  firstPicture;
    private String  flag;
    @Transient
    private String tagIds;
    private Integer views;
    private boolean appreciation;
    private boolean shareStatement; //转载声明
    private boolean commentabled;
    private boolean published;
    private boolean recommend;
    private String  blogInfo;
    @Temporal(TemporalType.TIMESTAMP) //时间格式转换
    private Date    createTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date    updateTime;
    @ManyToOne
    private Type type;

    @ManyToMany(cascade = {CascadeType.REFRESH})
    private List<Tag> tags = new ArrayList<Tag>();

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "blog")
    private List<Comment> comments = new ArrayList<>();
    public void init(){
        this.tagIds = tagsToIds(this.getTags());
    }
    private String tagsToIds(List<Tag> tags){
        if(!tags.isEmpty()){
            StringBuffer ids = new StringBuffer();
            boolean flag = false;
            for(Tag tag : tags){
                if(flag){
                    ids.append(",");
                }else{
                    flag = true;
                }
                ids.append(tag.getId());
            }
            return  ids.toString();
        }else{
            return  tagIds;
        }
    }

}
